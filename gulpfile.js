const gulp = require('gulp');

const path = require('path');

const merge = require('merge-stream');
const wrap = require('gulp-wrap');
const declare = require('gulp-declare');
const concat = require('gulp-concat');
const typescript = require('gulp-typescript');

function cprPromise(src, dest, options) {
    const cpr = require('cpr');

    return new Promise(function(resolve, reject) {
        cpr(src, dest, options, function(err, files) {
            if (err) {
                reject(err);
            } else {
                resolve(files);
            }
        })
    });
}

function copyOtherFiles(deleteFirst) {
    // Copy files to dist folder
    return cprPromise('.', 'dist', {
        deleteFirst: deleteFirst, // Delete "to" before 
        overwrite: true, // If the file exists, overwrite it 
        filter: /(^dist|^typings|^package.json|^package-lock.json|^\..*|^node_modules|^etc|^webpack.config|^web.config|^gulpfile.js|^tsconfig.json|^.*\.ts$|^.*\.scss$|^.*\.hbs$)/
    }).then(function() {
        // Copy typings
        return cprPromise('src/typings', 'dist/typings', {
            deleteFirst: deleteFirst,
            overwrite: true,
        });
    });
}

gulp.task('copy-files-clean', function() {
    return copyOtherFiles(true);
});

gulp.task('copy-files', function() {
    return copyOtherFiles(false);
});

gulp.task('sass', function () {
    const sass = require('gulp-sass');
    const autoprefixer = require('gulp-autoprefixer');

    return gulp.src('./{templates,css,scss,styles}/**/*.scss')
        .pipe(sass.sync({
            outputStyle: "expanded",
            indentWidth: 4
        })
        .on('error', function (error) {
            sass.logError(error);
            process.exit(1);
        }))
        .pipe(autoprefixer({
            browsers: [
              'last 2 versions',
              'Chrome >= 30',
              'Firefox >= 30',
              'ie >= 10',
              'Safari >= 8']
        }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('templates', function () {
    const handlebars = require('gulp-handlebars');

    var partials = gulp.src(['templates/**/*.hbs'])
      .pipe(handlebars({
          handlebars: require('handlebars')
      }))
      .pipe(wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
          imports: {
              processPartialName: function (fileName) {
                  // Strip the extension
                  // Escape the output with JSON.stringify 
                  return JSON.stringify(path.basename(fileName, '.js'));
              }
          }
      }));

    var templates = gulp.src('templates/**/*.hbs')
    .pipe(handlebars({
        handlebars: require('handlebars')
    }))
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
        namespace: 'PuFeLabs.handlebars.templates',
        noRedeclare: true, // Avoid duplicate declarations 
    }));

    // Output both the partials and the templates as build/js/templates.js
    return merge(partials, templates)
      .pipe(concat('templates.js'))
      .pipe(gulp.dest('dist/scripts'));
});

gulp.task('typescript', function () {
    const sourcemaps = require('gulp-sourcemaps');
    const tsProject = typescript.createProject('tsconfig.json');

    // Check if running in production mode
    if (process.env.PITLABS_ENV === 'production') {
        return tsProject.src()
            .pipe(tsProject(typescript.reporter.longReporter()))
            .on('error', function (error) {
                process.exit(1);
            })
            .js
            .pipe(gulp.dest('dist'));
    } else {
        // Development mode (default), generate sourcemaps
        return tsProject.src()
            .pipe(sourcemaps.init()) // Generate source maps as well
            .pipe(tsProject(typescript.reporter.longReporter()))
            .on('error', function (error) {
                process.exit(1);
            })
            .js
            .pipe(sourcemaps.write({
                sourceRoot: "../."
            }))
            .pipe(gulp.dest('dist'));
    }
});

gulp.task('ts-definitions', function () {
    const tsProject = typescript.createProject('tsconfig.json', { declaration: true });

    return tsProject.src()
        .pipe(tsProject(typescript.reporter.longReporter()))
        .on('error', function (error) {
            process.exit(1);
        })
        .dts
        .pipe(gulp.dest('dist/typings'));
});

gulp.task('watch', function () {
    // gulp.watch('./{templates,css,scss}/**/*.scss', gulp.series('sass'));
    // gulp.watch('templates/**/*.hbs', gulp.series('templates'));
    // gulp.watch('**/*.ts', gulp.series('typescript'));

    // TODO Ignoring the dist folder currently does not work
    // gulp.watch('**/*.*', { ignore: 'dist/**/*.*' }, gulp.series('copy-files'));
});


gulp.task('build', gulp.series(
    'copy-files-clean',
    gulp.parallel(
        'typescript', 
        'sass', 
        'templates',
        'ts-definitions'
    )
));